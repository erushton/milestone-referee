require 'gitlab'

require_relative './issue.rb'
require_relative './links.rb'
require_relative './milestone.rb'
require_relative './planning.rb'
require_relative './team.rb'

INSTANCE = "http://gitlab.com/"

CLIENT = Gitlab.client(
      endpoint: INSTANCE + 'api/v4',
      private_token: ENV['GL_TOKEN'],
      )

