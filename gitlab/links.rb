module Links

def self.missedSLOLabel
    "&label_name%5B%5D=missed-SLO"
end
def self.infradev
    "https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=infradev&first_page_size=20"
end

def self.infradev_pastdue
    self.infradev + self.missedSLOLabel
end

def self.security
    "https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=security&first_page_size=20"
end

def self.security_pastdue
    self.security + self.missedSLOLabel
end

def self.corrective_actions
    "https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=corrective%20action&first_page_size=20"
end

def self.corrective_actions_pastdue
    self.corrective_actions + self.missedSLOLabel
end

end
