require 'date'

def current_milestone_by_date(offset=0)
    milestone = Struct.new(:title, :start_date, :release_date)
    milestones = []
  
    milestones << milestone.new("15.0", Date.parse("2022-04-23"), Date.parse("2022-05-22"))
    milestones << milestone.new("15.1", Date.parse("2022-05-23"), Date.parse("2022-06-22"))
    milestones << milestone.new("15.2", Date.parse("2022-06-23"), Date.parse("2022-07-22"))
    milestones << milestone.new("15.3", Date.parse("2022-07-23"), Date.parse("2022-08-22"))
    milestones << milestone.new("15.4", Date.parse("2022-08-23"), Date.parse("2022-09-22"))
    milestones << milestone.new("15.5", Date.parse("2022-09-23"), Date.parse("2022-10-22"))
    milestones << milestone.new("15.6", Date.parse("2022-10-23"), Date.parse("2022-11-22"))
    milestones << milestone.new("15.7", Date.parse("2022-11-23"), Date.parse("2022-12-22"))
    milestones << milestone.new("15.8", Date.parse("2022-12-23"), Date.parse("2023-01-22"))
    milestones << milestone.new("15.9", Date.parse("2023-01-23"), Date.parse("2023-02-22"))
    milestones << milestone.new("15.10", Date.parse("2023-02-23"), Date.parse("2023-03-22"))
    
    day_in_milestone = Date.today
    loop do
      if offset == 0 || offset == nil
        break
      end
      if offset < 0
        day_in_milestone = day_in_milestone.prev_month
        offset += 1
      else
        day_in_milestone = day_in_milestone.next_month
        offset -= 1
      end
    end
    
    milestones.each { |m| 
      if m.start_date <= day_in_milestone && m.release_date >= day_in_milestone 
        return m.title
      end
    }
  
    raise "Milestone not found - probably need to update the lookup table"
  
end
