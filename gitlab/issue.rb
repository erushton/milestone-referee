class Issue
    attr_accessor :project
    attr_accessor :id
  
    def initialize(ihash)
      if ihash != nil
        @raw = ihash
        @id = ihash.iid
        @project = ihash.references.full.split('#')[0]
      end
    end
  
    def web_url()
      if @raw != nil
        return @raw.web_url
      else
        return INSTANCE + project + "/-/issues/" + id.to_s
      end
    end
  
    def get_raw()
      if @raw == nil
        @raw = CLIENT.issue(self.project, self.id)
      else
        return @raw
      end
    end
  
    def state()
      return get_raw.state
    end
  
    def milestone()
      if get_raw.milestone != nil 
        return get_raw.milestone.title
      else 
        return ""
      end
    end
  
    def open?()
      return get_raw.state == "opened"
    end
  
    def to_s()
      return web_url.blue + " - " + state.green + " - " + milestone.yellow
    end
  
    def eql?(other_issue)
      self.hash == other_issue.hash
    end
  
    def hash()
      # This feels error prone
      project.length * id.to_i
    end

    def severity_label()
      get_raw.labels.each { |l| 
        if l =~ /severity::/
          return '~"' + l + '"'
        end
      }

      return ""
    end

    def incident_summary()
      summary = "[" + get_raw.title + "](" + get_raw.web_url + ") " + severity_label
    end

    def hiring_label()
      get_raw.labels.each { |l| 
        if l =~ /Hiring::/
          return '~"' + l + '"'
        end
      }

      return ""
    end

    def hiring_req_summary()
      summary = "[" + get_raw.title + "](" + get_raw.web_url + ") " + hiring_label
    end

    def update_description(desc)
      CLIENT.edit_issue(@project, @id, { description: desc})
    end
  end