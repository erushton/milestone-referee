
def get_current_planning_issue(offset=0)
    cur_milestone = current_milestone_by_date(offset)
  
    # https://gitlab.com/api/v4/issues?scope=all&labels=group::runner&milestone=15.1&search=iteration%20plan&in=title
    planning_issues = CLIENT.issues("gitlab-org/gitlab-runner", {"scope" => "all",
    "labels" => "group::runner",
    "milestone" => cur_milestone,
    "search" => "iteration plan",
    "in" => "title"})
  
    # Might have multiple planning issues for given milestone because gitlab bot moves them forward if they aren't closed :facepalm:
  
    planning_issues.each { |pi|
      if pi.title.include? cur_milestone 
        return pi   
      end
    }
end
