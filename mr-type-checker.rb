require 'colorize'
require_relative './gitlab/gitlab.rb'

def get_open_mrs_for_milestone_with_no_type(offset=0)
    open_mrs = CLIENT.group_merge_requests("gitlab-org",
        {
            "scope" => "all",
            "state" => "opened",
            "labels" => "group::runner",
            "not[labels]" => "type::bug,type::maintenance,type::feature",
            "milestone" => current_milestone_by_date,
            "per_page" => 100,
        })

    return open_mrs
end

exit_code = 0

in_this_milestone = get_open_mrs_for_milestone_with_no_type
in_next_milestone = get_open_mrs_for_milestone_with_no_type(1)
in_last_milestone = get_open_mrs_for_milestone_with_no_type(-1)

all = in_this_milestone + in_next_milestone + in_last_milestone

if all.length != 0
    puts "The following MRs have no type\n".green
    all.each { |mr|
        puts mr.web_url.red
    }
    exit(all.length)
end

