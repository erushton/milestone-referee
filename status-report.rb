require 'colorize'
require 'date'

require_relative './gitlab/gitlab.rb'
require_relative './helpers/helpers.rb'

def weekly_update()
    return "## Weekly Updates\n\n"
end


def get_incidents_since_last_friday()
    # https://gitlab.com/api/v4/issues?scope=all&labels=incident,Service%3A%3ACI%20Runners&created_after=2022-06-03T00:00:00Z
    raw_incidents = CLIENT.issues("",
    {
        "scope" => "all",
        "labels" => "incident,Service::CI Runners",
        "created_after" => last_friday
    })

    incidents = []
    raw_incidents.each { |r| 
        incidents << Issue.new(r)
    }
    
    return incidents
end

def incident_report()
    incidents = get_incidents_since_last_friday

    report = "### Incidents :rotating_light:\n\n"
    if incidents.length > 0
        incidents.each { |i|
            report << " * " + i.incident_summary + "\n"
        }
    else
        report << "No incidents since last Friday :tada:\n\n"
    end

    return report + "\n"
end

def error_budget_report()
    puts "TODO - generate error budget report\n\n"
    return "### Error Budget\n\n<ERROR BUDGET>\n\n"
end

def missed_and_summary(label)
    total = CLIENT.issues("",
    {
        "scope" => "all",
        "state" => "opened",
        "labels" => "group::runner," + label,
    })
    if total.length == 0
        past_due = 0
    else
        past_due_issues = CLIENT.issues("",
        {
            "scope" => "all",
            "state" => "opened",
            "labels" => "group::runner,missed-SLO," + label,
        })
        past_due = past_due_issues.length
    end

    return total.length, past_due
end

def corrective_action_summary() 
    total, past_due = missed_and_summary("corrective action")
    return " * :pencil2:  Corrective Actions ([Total: %d](%s), [Past Due: %d](%s))\n" % 
        [total, Links.corrective_actions, past_due, Links.corrective_actions_pastdue]
end

def infradev_summary()
    total, past_due = missed_and_summary("infradev")
    return " * :construction: Infradev ([Total: %d](%s), [Past Due: %d](%s))\n" % [total, Links.infradev, past_due, Links.infradev_pastdue]
end

def security_summary()
    total, past_due = missed_and_summary("security")
    return " * :closed_lock_with_key: Security ([Total: %d](%s), [Past Due: %d](%s))\n" % [total, Links.security, past_due, Links.security_pastdue]
end

def reliability_report()
    # ### Issues related to Reliability 
    # <!-- You could add links to these queries so it's a matter of counting totals -->
    # - Infradev (Total: x, Past Due: y)
    # - Availability (Total: x, Past Due: y)
    # - Security (Total: x, Past Due: y)
    # - Corrective Action (Total: x, Past Due: y)

    report = "### Issues related to Reliability :building_construction:\n\n"
    
    report << infradev_summary
    report << security_summary
    report << corrective_action_summary
    report << "\n\n "
    return report
end

def hiring_report()
    reqs = CLIENT.issues("gitlab-com/ops-sub-department/ops-hiring-req-tracking", {
        "scope" => "all",
        "state" => "opened",
        "assignee_username" => Team.engineering_manager   
    })
    
    report = "### Hiring\n\n"

    if reqs.empty?
      report << " * No currently open roles\n"
    end

    reqs.each { |r| 
        i = Issue.new(r)
        report << " * " + i.hiring_req_summary + "\n"
    }

    return report + "\n\n"
end

def milestone_tracking
    # ## Milestone Tracking
    # <!-- For example: list issues or epics being tracked, and % of completion. Add overall health status for the milestone (e.g. committed vs planned) -->
    # - On Track | Needs Attention | At Risk 
    #   - x% of completion of Y
    #   - Managed [#incident-ABCDE]() and was not able to prioritize `fgh`

    report = "## Milestone Tracking\n\n"

    pi = Issue.new(get_current_planning_issue)

    report << "[%s Planning Issue](%s)\n\n" % [current_milestone_by_date, pi.web_url]


    return report
end

def okr_report()
    puts "TODO - generate okr report\n\n"
    return "### OKRs \n\n<OKRs>\n\n"
end



def generate_report(manual_error_report=nil, manual_okr_report=nil)
    report = ""

    report << milestone_tracking
    
    report << weekly_update
    report << incident_report

    if manual_error_report == nil 
        report << error_budget_report
    else
        report << manual_error_report
    end

    report << reliability_report
    
    if manual_okr_report == nil 
        report << okr_report
    else
        report << manual_okr_report
    end
    
    report << hiring_report

    return report
end

if __FILE__ == $0
    puts generate_report
end


# ## Announcements

# <!-- Accomplishments, things to celebrate, tasks to-do (recurring or added) -->

# ## Milestone Tracking
# <!-- For example: list issues or epics being tracked, and % of completion. Add overall health status for the milestone (e.g. committed vs planned) -->
# - On Track | Needs Attention | At Risk 
#   - x% of completion of Y
#   - Managed [#incident-ABCDE]() and was not able to prioritize `fgh`

# ## Weekly Updates

# ### Incidents (if any)

# <!-- List of incident(s) raised that were related to the team's domain -->

# ### Error Budget

# <!-- Can be a copy/paste from the Slack update -->

# ### Issues related to Reliability 

# <!-- You could add links to these queries so it's a matter of counting totals -->
# - Infradev (Total: x, Past Due: y)
# - Availability (Total: x, Past Due: y)
# - Security (Total: x, Past Due: y)
# - Corrective Action (Total: x, Past Due: y)

# ### OKRs

# <!-- Link to Ally OKRs, provide an updated score -->

# ### Hiring 

# <!-- List any open reqs and each of their status -->
