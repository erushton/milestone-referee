## Milestone Auditor

Checks that there are no issues assigned to the current milestone that aren't in our current milestones planning issue.

### Rough outline of what it does:

1. Determine current milestone value; if can't determine, fail
1. Find milestone issue; if multiple fail
1. Get the list of all issues in the planning issue description
1. Get the list of all group issues in the gitlab-org namespace for this milestone
1. If any are in the milestone and not the planning issue, list them, make sure job is failed
1. If there are any in the PI but not assigned to this milestone, list them, fail the job
