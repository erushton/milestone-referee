require 'date'

def last_friday()
    d = Date.today

    # If today is friday, we don't want this date, so
    # skip back to thursday to then find the date of
    # previous friday.
    if d.friday?
        d -= 1
    end

    loop do
        if d.friday?
            break
        end
        d -= 1
    end
    return d.to_datetime.iso8601
end

if __FILE__ == $0
    puts last_friday
end