## Errors & Availability

* Last week's service availability was 99.95% 
* For June 2022, the total error budget spent is 45.9 minutes which is 110% over budget. The error budget overage and impact on the SLO for July is due to planned downtime for maintenance work on the weekend of July 2nd.


<!-- Error tracking dashboard https://dashboards.gitlab.net/d/general-slas/general-slas?from=now%2FM&orgId=1&to=now&viewPanel=21 -->
<!-- Apdex https://dashboards.gitlab.net/d/ci-runners-main/ci-runners-overview?viewPanel=79474957&orgId=1&from=1654473600000&to=1654905599000  -->
