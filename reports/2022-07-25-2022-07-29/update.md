## Announcements

* Due to a [regression](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29193) caused by a dependency upgrade in `15.2.0` we rolled out a patch release containing just the rollback of the dependency upgrade as `15.2.1`


