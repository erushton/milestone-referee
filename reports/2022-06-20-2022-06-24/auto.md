## Milestone Tracking

[15.2 Planning Issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28888)

## Weekly Updates

### Incidents :rotating_light:

 * [2022-06-23: CiRunnersServiceQueuingQueriesDurationApdexSLOViolation](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7316) ~"severity::2"
 * [2022-06-20: Disk space approaching limits on prometheus-01-gitlab-runners](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7274) ~"severity::4"

## Errors & Availability

* Last week's average service availability was 99.96% 
* For June 2022, the total error budget spent is 0 minutes!


<!-- Error tracking dashboard https://dashboards.gitlab.net/d/general-slas/general-slas?from=now%2FM&orgId=1&to=now&viewPanel=21 -->
<!-- Apdex https://dashboards.gitlab.net/d/ci-runners-main/ci-runners-overview?viewPanel=79474957&orgId=1&from=1654473600000&to=1654905599000  -->
### Issues related to Reliability :building_construction:

 * :construction: Infradev ([Total: 1](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=infradev&first_page_size=20), [Past Due: 1](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=infradev&first_page_size=20&label_name%5B%5D=missed-SLO))
 * :closed_lock_with_key: Security ([Total: 18](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=security&first_page_size=20), [Past Due: 3](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=security&first_page_size=20&label_name%5B%5D=missed-SLO))
 * :pencil2:  Corrective Actions ([Total: 11](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=corrective%20action&first_page_size=20), [Past Due: 0](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=corrective%20action&first_page_size=20&label_name%5B%5D=missed-SLO))


 

### Hiring

 * [Backend Engineer, Runner (2065)](https://gitlab.com/gitlab-com/ops-sub-department/ops-hiring-req-tracking/-/issues/14) ~"Hiring::Job Posting Published"
 * [Sr. Backend Engineer,Runner (1993)](https://gitlab.com/gitlab-com/ops-sub-department/ops-hiring-req-tracking/-/issues/10) ~"Hiring::Job Posting Published"


