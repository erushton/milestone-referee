## Milestone Tracking

[15.1 Planning Issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28861)

## Weekly Updates

### Incidents :rotating_light:

 * [2022-06-13: Multiple QA Sanity Failures for Production Canary](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/7243) ~"severity::3"

## Errors & Availability

* Last week's service availability was 100.0% :tada:
* For June 2022, the total error budget spent is 0 minutes! :100:

<!-- Error tracking dashboard https://dashboards.gitlab.net/d/general-slas/general-slas?from=now%2FM&orgId=1&to=now&viewPanel=21 -->
<!-- Apdex https://dashboards.gitlab.net/d/ci-runners-main/ci-runners-overview?viewPanel=79474957&orgId=1&from=1654473600000&to=1654905599000  -->
### Issues related to Reliability :building_construction:

 * :construction: Infradev ([Total: 1](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=infradev&first_page_size=20), [Past Due: 1](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=infradev&first_page_size=20&label_name%5B%5D=missed-SLO))
 * :closed_lock_with_key: Security ([Total: 19](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=security&first_page_size=20), [Past Due: 3](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=security&first_page_size=20&label_name%5B%5D=missed-SLO))
 * :pencil2:  Corrective Actions ([Total: 10](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=corrective%20action&first_page_size=20), [Past Due: 0](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=corrective%20action&first_page_size=20&label_name%5B%5D=missed-SLO))


 
## OKRs

to-be-updated
### Hiring

 * [Backend Engineer, Runner (2065)](https://gitlab.com/gitlab-com/ops-sub-department/ops-hiring-req-tracking/-/issues/14) ~"Hiring::Job Posting Published"
 * [Sr. Backend Engineer,Runner (1993)](https://gitlab.com/gitlab-com/ops-sub-department/ops-hiring-req-tracking/-/issues/10) ~"Hiring::Job Posting Published"


