## Announcements

* On Thursday we ran the [script on .com to prune stale runners](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5910#note_993515307) (runners that hadn't contacted .com in over 3-months). Due to a copy and paste error, this ran against any projects with 10 or more stale runners depsite it being intended to run against projects with 1000 or more stale runners. There should be no functional impact to users or customers as these runners are by definition stale and not processing jobs. Support has been made aware to keep an eye out but so far we have heard nothing.

* :tada: We are fully on track for launching macOS Shared Runners to Limited Availability as part of the %15.1 release.


