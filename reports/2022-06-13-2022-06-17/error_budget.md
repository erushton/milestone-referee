## Errors & Availability

* Last week's service availability was 100.0% :tada:
* For June 2022, the total error budget spent is 0 minutes! :100:

<!-- Error tracking dashboard https://dashboards.gitlab.net/d/general-slas/general-slas?from=now%2FM&orgId=1&to=now&viewPanel=21 -->
<!-- Apdex https://dashboards.gitlab.net/d/ci-runners-main/ci-runners-overview?viewPanel=79474957&orgId=1&from=1654473600000&to=1654905599000  -->
