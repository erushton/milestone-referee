## Announcements

* :cloud: [Multiple machine types](https://about.gitlab.com/blog/2022/09/22/new-machine-types-for-gitlab-saas-runners/)  was launched for Runner SaaS! This adds the option of 2-vCPU and 4-vCPU machines on the .com shared runners for customers to use and paves the way for additional machine types in the future.


<!-- Accomplishments, things to celebrate, tasks to-do (recurring or added) -->

