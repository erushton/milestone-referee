## Milestone Tracking

[15.5 Planning Issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28975)

## Weekly Updates

### Incidents :rotating_light:

No incidents since last Friday :tada:


## Errors & Availability

* Last week's service availability was 99.99%
* Error Budget: With 1.5 hours spent, the CI Runners availability for September was 99.79%, which is, of course, below the goal of 99.95%.

<!-- Error tracking dashboard https://dashboards.gitlab.net/d/general-slas/general-slas?from=now%2FM&orgId=1&to=now&viewPanel=21 -->
<!-- Apdex https://dashboards.gitlab.net/d/ci-runners-main/ci-runners-overview?viewPanel=79474957&orgId=1&from=1654473600000&to=1654905599000  -->
### Issues related to Reliability :building_construction:

 * :construction: Infradev ([Total: 1](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=infradev&first_page_size=20), [Past Due: 1](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=infradev&first_page_size=20&label_name%5B%5D=missed-SLO))
 * :closed_lock_with_key: Security ([Total: 20](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=security&first_page_size=20), [Past Due: 3](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=security&first_page_size=20&label_name%5B%5D=missed-SLO))
 * :pencil2:  Corrective Actions ([Total: 11](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=corrective%20action&first_page_size=20), [Past Due: 0](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=corrective%20action&first_page_size=20&label_name%5B%5D=missed-SLO))


 ### OKRs 

<OKRs>

### Hiring

 * No currently open roles


