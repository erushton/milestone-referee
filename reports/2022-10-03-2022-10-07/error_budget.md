## Errors & Availability

* Last week's service availability was 99.99%
* Error Budget: With 1.5 hours spent, the CI Runners availability for September was 99.79%, which is, of course, below the goal of 99.95%.

<!-- Error tracking dashboard https://dashboards.gitlab.net/d/general-slas/general-slas?from=now%2FM&orgId=1&to=now&viewPanel=21 -->
<!-- Apdex https://dashboards.gitlab.net/d/ci-runners-main/ci-runners-overview?viewPanel=79474957&orgId=1&from=1654473600000&to=1654905599000  -->
