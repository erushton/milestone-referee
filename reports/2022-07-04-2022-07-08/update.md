## Announcements

* We had a [SIRT issue](https://gitlab.com/gitlab-sirt/incident_2382/-/issues/1) discovered at the start of the week. After thorough analysis by myself, Touni, and Georgi we were able to prove that no compromise had occurred. From a Development point of view the "clean up" is now done on the incident. We will need to restore one pipeline job that was removed but this is no rush. (I'm being some what purposefully vague - more details are in the incident issue).
* Our Apdex/downtime is erroneously wrong due to the planned GitLab.com outage on the weekend. I have not included it in this status update issue. For the rest of the month I will but with the first few days of the month taken off to avoid includin the planned downtime in the scores.
* Hiring wise we have some very strong candidates at later stages, one of which I just completed reference checks on.



<!-- Accomplishments, things to celebrate, tasks to-do (recurring or added) -->

