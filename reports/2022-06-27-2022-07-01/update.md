## Announcements

* Welcome to the team @avonbertoldi! :tada:
* We had a significant [regression](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29141) get out ahead of our planned 15.1.1 security release. It affected anyone installing GitLab-Runner using the `latest` tag. We've revoked the packages and made `latest` point back to the previous good release. We'll also not be taking part in the security release as a result. Stay tuned for more of an update including corrective actions.
 
<!-- Accomplishments, things to celebrate, tasks to-do (recurring or added) -->

