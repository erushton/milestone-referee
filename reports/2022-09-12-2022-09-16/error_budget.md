* Availability: For the work-week ending 2022-09-02, the average CI Runners availability is 99.99%.
* Error Budget: The Error budget spent month-to-date is 47.7 minutes due to the 2022-09-03 maintenance on GitLab.com.
