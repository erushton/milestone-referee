## Milestone Tracking

[15.4 Planning Issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28943)

## Weekly Updates

### Incidents :rotating_light:

No incidents since last Friday :tada:


* Availability: For the work-week ending 2022-09-02, the average CI Runners availability is 99.99%.
* Error Budget: The Error budget spent month-to-date is 47.7 minutes due to the 2022-09-03 maintenance on GitLab.com.
### Issues related to Reliability :building_construction:

 * :construction: Infradev ([Total: 1](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=infradev&first_page_size=20), [Past Due: 1](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=infradev&first_page_size=20&label_name%5B%5D=missed-SLO))
 * :closed_lock_with_key: Security ([Total: 20](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=security&first_page_size=20), [Past Due: 2](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=security&first_page_size=20&label_name%5B%5D=missed-SLO))
 * :pencil2:  Corrective Actions ([Total: 11](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=corrective%20action&first_page_size=20), [Past Due: 0](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Arunner&label_name%5B%5D=corrective%20action&first_page_size=20&label_name%5B%5D=missed-SLO))


 ### Hiring

 * [Backend Engineer, Runner (2065)](https://gitlab.com/gitlab-com/ops-sub-department/ops-hiring-req-tracking/-/issues/14) ~"Hiring::Interviewing"
 * [Sr. Backend Engineer,Runner (1993)](https://gitlab.com/gitlab-com/ops-sub-department/ops-hiring-req-tracking/-/issues/10) ~"Hiring::Interviewing"


