require 'colorize'
require_relative './gitlab/gitlab.rb'

def get_issues_assigned_to_milestone(offset=0)

  pg = 1

  milestone_issues = []

  loop do
    page_of_issues = CLIENT.group_issues("gitlab-org", {"scope" => "all",
    "labels" => "group::runner",
    "state" => "opened",
    "per_page" => 100,
    "page" => pg,
    "milestone" => current_milestone_by_date(offset)})

    milestone_issues += page_of_issues

    if page_of_issues.length < 100
      break
    end

    pg += 1

  end

  issues = []
  milestone_issues.each { |m| 
    issues << Issue.new(m)
  }
  return issues
end

def get_issues_from_planning_issue(offset=0) 
  short_format = /(\S*)\#(\d+)/
  planning_issue = get_current_planning_issue(offset)
  raw_issues_in_pi = planning_issue.description.scan(short_format)
  
  issues_in_pi = []
  
  raw_issues_in_pi.each { |i| 
    issue = Issue.new(nil)
    
    # Skip things like "see issue #12345"
    if (i[0] =~ /\w+/) == nil
      next
    end
    issue.project = i[0]
    issue.id = i[1]
    issues_in_pi << issue
  }

  return issues_in_pi
end

milestone_offset = ARGV[0].to_i

issues_in_pi = get_issues_from_planning_issue(milestone_offset)
issues_in_milestone = get_issues_assigned_to_milestone(milestone_offset)


open_issues_in_pi = issues_in_pi.select {|i| i.open? }
in_pi_not_in_milestone = open_issues_in_pi - issues_in_milestone

puts "Generating report for milestone ".green + current_milestone_by_date((milestone_offset)).yellow

if milestone_offset >= 0
  if in_pi_not_in_milestone.length > 0 
    puts "The following ".red + in_pi_not_in_milestone.length.to_s + " open issues are in the Planning Issue but not assigned to the correct milestone".red
    in_pi_not_in_milestone.each { |i| 
      puts i
    }
    puts ""
  end
end

in_milestone_not_in_pi = issues_in_milestone - issues_in_pi
if in_milestone_not_in_pi.length > 0 
  puts "The following ".red + in_milestone_not_in_pi.length.to_s + " issues are in the milestone but not assigned in the Planning Issue".red
  in_milestone_not_in_pi.each { |i| 
    puts i
  }
  puts ""
end

exit(in_pi_not_in_milestone.length + in_milestone_not_in_pi.length)
