require 'date'
require 'fileutils'

require_relative '../gitlab/gitlab.rb'
require_relative '../helpers/helpers.rb'

def project_to_use()
    if ENV["DEBUG"] == nil # not debug
        return 14146123
    else
        # Debug mode
        return 11269441
    end
end

class Report
    attr_accessor :issue, :error_reporting, :okr_reporting

    def initialize(ri)
        @issue = Issue.new(ri)

        determine_dates
        @path = local_path

        init_local_files
    end

    def init_local_files()
        if File.exists?(@path)
            return
        end

        Dir.mkdir(@path)
        FileUtils.cp('reports/template.md', @path + '/update.md')
    end

    def determine_dates()
        today = Date.today
        @monday = today - (today.wday - 1)
        @friday = today + (5 - today.wday)
    end

    def local_path()
        date_fmt = "%Y-%m-%d"
        return "reports/%s-%s/" % [@monday.strftime(date_fmt), @friday.strftime(date_fmt)]
    end

    def update_file()
        return @path + "/update.md"
    end

    def auto_file()
        return @path + "/auto.md"
    end

    def prep_error_file()
        if !File.exists?(error_file)
            FileUtils.cp('reports/error_template.md', error_file)
        end
    end

    def error_file()
        return @path + "/error_budget.md"
    end

    def get_error_report()
        if self.error_reporting == false
          return nil
        end
        inf = File.open(error_file, "r")
        return inf.read
    end

    def okr_file()
        return @path + "/okrs.md"
    end

    def get_okr_report()
        if self.okr_reporting == false
          return nil
        end
        inf = File.open(okr_file, "r")
        return inf.read
    end

    def prep_okr_file()
        if !File.exists?(okr_file)
            FileUtils.cp('reports/okr_template.md', okr_file)
        end
    end

    def save_auto(auto_report)
        # Save the generated auto report to file
        output = File.open(auto_file, "w")
        output << auto_report
        output.close
    end

    def full_report()
        mf = File.open(update_file, "r")
        manual = mf.read
        mf.close

        af = File.open(auto_file, "r")
        auto = af.read
        af.close

        return manual + "\n\n" + auto
    end

    def update_report_description()
        r = full_report
        @issue.update_description(r)
    end

    def update_aborted?()
        cmd = 'git status -s | grep "M\|A\|R\|??"'
        return !system(cmd)
    end

end

def get_this_weeks_report_issue()
    report_issues = CLIENT.issues(project_to_use,
    {
        "scope" => "all",
        "state" => "opened",
        "created_after" => last_friday,
        "search" => "Verify:Runner EM update",
        "in" => "title"
    })

    if report_issues.length < 1 
        close_old_report_issues
        return create_this_weeks_report_issue
    elsif report_issues.length > 1
        raise "Found too many report issues. Can't confidently tell whats going on"
    else # Found just 1 issue
        return report_issues[0]
    end
end

def create_this_weeks_report_issue()
    today = Date.today
    title = "W%s Verify:Runner EM update" % today.cweek
    return CLIENT.create_issue(project_to_use, title,
    {
        confidential: true,
        labels: "OpsSection::Weekly-Update,section::ops,devops::verify,group::runner",
    })
end

def close_old_report_issues()
    report_issues = CLIENT.issues(project_to_use,
    {
        "scope" => "all",
        "state" => "opened",
        "created_before" => last_friday,
        "search" => "Verify:Runner EM update",
        "in" => "title"
    })

    report_issues.each { |ri| 
        CLIENT.close_issue(project_to_use, ri.iid)
    }
end



report = Report.new(get_this_weeks_report_issue)

report.error_reporting = false
report.okr_reporting = false

ARGV.each { |arrrrg|
    arg = arrrrg.upcase
    if ["ERROR_BUDGET", "ERRORBUDGET", "ERRORS", "ERROR"].include? arg
        report.prep_error_file
        system("vi #{report.error_file}")
        report.error_reporting = true
    end

    if ["OKR", "OKRS", "OKRUPDATE"].include? arg
        report.prep_okr_file
        system("vi #{report.okr_file}")
        report.okr_reporting = true
    end
}

system("vi #{report.update_file}")

# Check if update.md was actually modified. If not, don't push update
# this allows us to exit vi ie: `q!` and abort changes
if report.update_aborted? 
    puts "Aborting script - no updates to update"
    exit(1)
end

# Run status report script
require_relative '../status-report.rb'
auto_report = generate_report(report.get_error_report, report.get_okr_report)

report.save_auto(auto_report)

# Get contents of manual and auto updates, concat them
# Post the updates to the issue
report.update_report_description

puts report.issue.web_url
